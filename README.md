# wdg-boilerplate

Esto es un boilerplate para crear APIs de Juegos para The Distributed Game con node.js

## .env

- Crea un archivo .env en la raíz de este proyecto y completá
```
DB_HOST=host
DB_USER=user
DB_PASSWORD=password
DB_NAME=name
```

Si estás usando Toolforge, consultá https://wikitech.wikimedia.org/wiki/Help:Toolforge/Database para obtenerlos.

## Base de datos
- Necesitarás una tabla para guardar tus tiles. Este SQL crea una tabla de tiles básica.
```
CREATE TABLE `tiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT, /* Id de tu tile. Puede coincidir con el qid */
  `qid` varchar(100) NOT NULL, /* Qid del elemento a editar */
  `nombre` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0 CHECK (`status` in (0,1,2)), /* Estado de la tile: pendiente, completo o inválido */
  `tipo` int(11) NOT NULL, /* Opcional: columnas que te ayudarán a determinar qué opciones verdes mostrar  */
  `usuario` varchar(100) DEFAULT NULL /* Si quieres guardar quién completó la tile */
  PRIMARY KEY (`id`)
)
```
