require('dotenv').config({path:__dirname + '/.env'});
const express = require('express')
const mysql = require('mysql')
const connection = mysql.createConnection({
  host: `${process.env.DB_HOST}`,
  user: `${process.env.DB_USER}`,
  password: `${process.env.DB_PASSWORD}`,
  database: `${process.env.DB_NAME}`
})
connection.connect();

//Status de los tiles.
const STATUS_NOT_PLAYED = 0;
const STATUS_SOLVED = 1;
const STATUS_INVALID = 2;

const app = express()
const port = process.env.PORT || 3000;

app.get('/', handleRequest)

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

 async function createTiles(rows) {
  let tiles = await Promise.all(
    rows.map(async(item) => {
      let entries = await createGreenOptions(item)
      entries.push({ "type": "white", "decision": "skip", "label": "Siguiente" });
      entries.push({ "type": "blue", "decision": "no", "label": "Ninguna opción es la correcta" });
      return {
        id: item.qid,
        //secciones posibles: item, text y wikipage. más info: https://bitbucket.org/magnusmanske/wikidata-game/src/master/public_html/distributed/readme.md?at=master
        sections: [ {
          "type": "item",
          "q": item.qid
        }],
        controls: [{
            "type": "buttons",
            "entries": entries
        }]
      };
    })
  );
  return {
    "tiles" : tiles
  }
}

//crea las opciones válidas para cada tile
async function createGreenOptions(item){
  //puedes enviar las mismas opciones verdes a todos los tiles
  //puedes tener las opciones guardadas en otra tabla de tu bbdd
  //o algo más sofisticado

  //en este ejemplo, cada tile pertenece a un país, y las opciones verdes son ciudades de ese país
  //consultamos una tabla de ciudades para construir las opciones verdes
  let cities = await getCities(item.country)
  let options = cities.map(function (city, index) {
    let value_qid =city.qid.replace("Q","")//obtener el número de Qid, sin la Q
    return {
      "type": "green",
      "decision": "yes",
      "label": city.nombre, //etiqueta que aparecerá en el botón
      "api_action": {
          "action": "wbcreateclaim",
          "entity": item.qid, //qid del item que vas a modificar
          "property": "P19", //la propiedad que vas a modificar
          "snaktype": "value",
          "value": `{\"entity-type\":\"item\",\"numeric-id\":${value_qid}}` //el valor que le darás a esa propiedad
      }
    }
  });
  return options
}

async function getCities(country) {
  maxOptions = 10
  sqlQuery = `SELECT * 
    FROM ciudad
    WHERE pais = ${country}
    ORDER BY poblacion DESC
    LIMIT ${maxOptions};`

  let cities = await queryDatabase(sqlQuery)
  return cities
}

function queryDatabase(sqlQuery) {
  return new Promise((resolve, reject) => {
    connection.query(sqlQuery, (err, rows, fields) => {
      if (err) {
        console.log("QUERY FAILED")
        reject(err);
      } else {
        resolve(rows);
      }
    });
  });
}

//registramos que un usuario marca una tile como inválida
function logNo(id, user) {
  console.log("log no")
  sqlQuery = `UPDATE tiles
  SET status = ${STATUS_INVALID}, usuario = '${user}'
  WHERE qid = '${id}';`;
  console.log(sqlQuery)
  queryDatabase(sqlQuery);
}

//registramos que un usuario marca una opción correcta
function logYes(id, user) {
  console.log("log yes")
  sqlQuery = `UPDATE tiles
  SET status = ${STATUS_SOLVED}, usuario = '${user}'
  WHERE qid = '${id}';`;
  console.log(sqlQuery)
  queryDatabase(sqlQuery);
}

async function handleRequest(req, res) {
  try {
    const action = req.query.action;
    const callback = req.query.callback;
    switch (action) {
      case "desc":
        //título, descripción e ícono
        let descJson = {
          "label" : {
            "en" : "Name of The Game in English",
            "es" : "Nombre del Juego en Español",
            //agrega más idiomas
          },
          "description" : {
            "en" : "Game description in English. Give the players clear instructions and some context.",
            "es" : "Descripción del juego en español. Dale a les jugadores instrucciones claras y algo de contexto.",
            //agrega más idiomas
          },
          //reemplaza por otro icono bonito e ilustrativo
          "icon" : 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/WU_logo_3.png/640px-WU_logo_3.png'
        }
        res.send(`${callback}(${JSON.stringify(descJson)})`);
        break;
      
      case "tiles":
        console.log("requested tiles")
        //si la query no especifica cuantas tiles, enviaremos 5
        const num = req.query?.num ? req.query.num : 5;
        
        //traemos solo las tiles que no han sido jugadas
        //un orden random mitiga la posibilidad de que se jueguen dos tiles a la vez
        sqlQuery = `SELECT * 
        FROM tiles
        WHERE status = ${STATUS_NOT_PLAYED}
        ORDER BY RAND()
        LIMIT ${num};`

        //traemos los items
        let items = await queryDatabase(sqlQuery)
        //creamos los tiles
        let tiles = await createTiles(items);
        //enviamos los tiles
        res.send(`${callback}(${JSON.stringify(tiles)})`);
        break;

      case "log_action":
        let decision = req.query.decision
        let user = req.query.user
        let tile = req.query.tile
        
        if (decision=="yes") {
          logYes(tile,user)
        } else if (decision=="no"){
          logNo(tile,user)
        }
        break;
    
      default:
        res.json({
          error: 'Parámetro "action" inválido'
        });
    }
  } catch (error) {
    console.error('Uncaught Exception:', error);
  }
}

process.on('uncaughtException', (error) => {
  console.error('Uncaught Exception:', error);
  // Realizar acciones de limpieza si es necesario
  process.exit(1); // Forzar la salida de la aplicación con un código de error
});
